﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
namespace DAL.Gateway
{
    public class DBObject
    {
        private static readonly SqlConnection conn;
        static DBObject()
        {
            string conString = ConfigurationManager.AppSettings["DBCon"].ToString();
            conn = new SqlConnection(conString);
        }
        public static void OpenConnetion()
        {
            conn.Open();

        }
        public static void CloseConnection()
        {
            conn.Close();
        }
        public static SqlConnection Conn
        {
            get
            {
                return conn;
            }
        }
    }
}