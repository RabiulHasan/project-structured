﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using  System.Data;
using System.Data.SqlClient;
using DAL.Gateway;

namespace DAL.Gateway
{
    public class Transaction
    {
        SqlCommand cmd;
        SqlDataAdapter da;
        DataTable dt;
        public Transaction()
        { 
        }

        public void ExecuteStoreProcedureCommadn(string command)
        {
            try
            {
                DBObject.OpenConnetion();
                cmd = new SqlCommand(command, DBObject.Conn);
                cmd.CommandTimeout = 10000;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                DBObject.CloseConnection();
            }
        }
        public void ExecuteCommand(string command)
        {
            try
            {
                DBObject.OpenConnetion();
                cmd = new SqlCommand(command, DBObject.Conn);
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = 10000;
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                DBObject.CloseConnection();
            }
        }
        public string ExecuteScalar( string command)
        {
            string value = "";
            try
            {
                DBObject.OpenConnetion();
                cmd = new SqlCommand(command, DBObject.Conn);
                cmd.CommandTimeout = 10000;
                cmd.CommandType = CommandType.Text;

                value = cmd.ExecuteScalar().ToString();
                
            }
            catch (Exception ex)
            {

                //throw ex;
                value = "ERR";
            }
            finally
            {
                DBObject.CloseConnection();
            }
            return value;
        }
        public DataTable ExecteDatatable(string command)
        {
            try
            {
                DBObject.OpenConnetion();
                cmd = new SqlCommand(command, DBObject.Conn);
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = 10000;
                da = new SqlDataAdapter(cmd);
                dt = new DataTable();
                da.Fill(dt);
                return dt;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                DBObject.CloseConnection();
            }
        }
        public DataTable FetchData(string query)
        {
            try
            {
                DBObject.OpenConnetion();
                da = new SqlDataAdapter(query,DBObject.Conn);
                dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                DBObject.CloseConnection();
            }
            
        }





        public int Delete(string query)
        {
            try
            {
                DBObject.OpenConnetion();
                cmd=new SqlCommand(query, DBObject.Conn);
                cmd.CommandTimeout = 10000;
                cmd.CommandType = CommandType.Text;
                int result = cmd.ExecuteNonQuery();
                return result;

            }
            catch (Exception ex)
            {
                
                throw ex;
            }
            finally
            {
                DBObject.CloseConnection();
            }
        }
    }
}