﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="consolidete_emp.aspx.cs" Inherits="Structure.Reports.Rpt_parameter.consolidete_emp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
    <form id="form1" class="form-horizontal" runat="server">
        <div class="container">
            <br />
            <div class="panel panel-info">
                <div class="panel-body">
                     <div class="form-group">
                <label class="control-label col-md-2">Department</label>
                <div class="col-md-4">
                    <asp:DropDownList ID="ddDepartment" runat="server" CssClass="form-control"></asp:DropDownList>

                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2">Employee Id</label>
                <div class="col-md-4">
                    <asp:TextBox ID="txtEmpid" runat="server" CssClass="form-control">All</asp:TextBox>

                </div>
            </div>
                      <div class="form-group">
                <label class="control-label col-md-offset col-md-2">Employee Id</label>
                <div class="col-md-4">
                     <asp:Button ID="btn" runat="server" Text="Button" CssClass="btn btn-info" OnClick="btn_Click"/>

                </div>
                         
            </div>
                </div>

            </div>

           
        </div>
    </form>
</body>
</html>
