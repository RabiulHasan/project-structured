﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Structure.DLL;
using System.Data.SqlClient;
using DAL.Gateway;
using System.IO;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

namespace Structure.Reports.Rpt_parameter
{
    public partial class consolidete_emp : System.Web.UI.Page
    {
        Transaction objTran;
        Gatewaye objGate;
        SqlCommand cmd;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadDepartment();
            }

        }

        void LoadDepartment()
        {
            objTran = new Transaction();
            DataTable dt = objTran.FetchData("Select distinct category_id,category_name from department d inner join employee_info e on d.category_id=e.department");
            ddDepartment.DataSource = dt;
            ddDepartment.DataValueField = "category_id";
            ddDepartment.DataTextField = "category_name";
            ddDepartment.Items.Insert(0, new ListItem(" ", "--Select--"));
            ddDepartment.DataBind();
        }
        //void ShowReport()
        //{
        //    objGate = new Gatewaye();
        //    string report_path;
        //    DataTable dt = objGate.GetEmployeReport(ddDepartment.SelectedValue, txtEmpid.Text);
        //    if (dt.Rows.Count > 0) 
        //    {
        //        Reports.Rpt_Object.rpt_employeeInfo rpt = new Rpt_Object.rpt_employeeInfo();
        //        rpt.SetDataSource(dt);
        //        report_path = Server.MapPath("~/Reports"+ DateTime.Now + ".pdf");
        //        rpt.ExportToDisk(ExportFormatType.PortableDocFormat, report_path);

        //        string url = "/ReportViewer.aspx?path=" + report_path;
        //        string s = "window.open('" + url + "', 'popup_window', 'width=860,height=460,left=100,top=100,resizable=yes');";
        //        ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
        //    }

          
        //}

        void DisplayReport()
        {
            objTran = new Transaction();

           // SqlDataAdapter da = new SqlDataAdapter("rpt_sp_employeeinfo '" + ddDepartment.SelectedValue + "','" + txtEmpid.Text.Trim() + "'", DBObject.Conn);
            SqlDataAdapter da = new SqlDataAdapter("rpt_sp_allEmployee", DBObject.Conn);
            da.SelectCommand.CommandTimeout = 10000;
            try
            {
                DataSet ds = new DataSet();
                da.Fill(ds, "rpt_sp_allEmployee");
                ReportDocument rpt = new ReportDocument();
                rpt.Load(Server.MapPath("~/Reports/Rpt_Object/rpt_allEmp.rpt"));
                rpt.SetDataSource(ds);
                Session["_rpt"] = rpt;
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "NewWindow", "window.open('../PrintReport.aspx','_blank');", true);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            //finally
            //{
            //    da.Dispose();
            //}
        
        }
        protected void btn_Click(object sender, EventArgs e)
        {
            DisplayReport();
        }
    }
}