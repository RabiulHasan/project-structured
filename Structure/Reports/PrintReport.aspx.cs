﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Structure.Reports
{
    public partial class PrintReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void CrystalReportViewer1_Init(object sender, EventArgs e)
        {
            ReportDocument rd = new ReportDocument();
            rd = (ReportDocument)Session["_rpt"];
            if (ConfigurationManager.AppSettings["ReportMode"] == "PDF")
            {
                rd.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, false, "Test");

            }
            else
            {
                CrystalReportViewer1.ReportSource = rd;
            }
        }
    }
}