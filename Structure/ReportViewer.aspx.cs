﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using CrystalDecisions.Shared;

namespace Structure
{
    public partial class ReportViewer : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            
            //if (!IsPostBack)
            //{
            //    try
            //    {
            //        if (Request.QueryString["path"] != null)
            //        {
            //            string path = Request.QueryString["path"].ToString();

            //            FileInfo file = new FileInfo(path);
            //            Stream st = file.OpenRead();
            //            using (MemoryStream memoryStream = new MemoryStream())
            //            {

            //                st.CopyTo(memoryStream);
            //                byte[] bytes = memoryStream.ToArray();
            //                memoryStream.Close();
            //                Response.Clear();
            //                Response.Buffer = true;
            //                Response.ContentType = "application/pdf";
            //                Response.BinaryWrite(bytes);
            //                Response.End();

            //            }

            //        }
            //    }
            //    catch
            //    {

            //    }



            //}
        }

       
        protected void CrystalReportViewer1_Init(object sender, EventArgs e)
        {
            ReportDocument rd = new ReportDocument();
            rd = (ReportDocument)Session["_rpt"];
            if (ConfigurationManager.AppSettings["ReportMode"] == "PDF")
            {
                rd.ExportToHttpResponse(ExportFormatType.PortableDocFormat,Response,false,"Test");

            }
            else
            {
                CrystalReportViewer1.ReportSource = rd;
            }
        }
    }
}