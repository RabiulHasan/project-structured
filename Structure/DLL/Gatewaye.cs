﻿using Structure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using DAL.Gateway;
using System.Data;
using System.Configuration;
using System.Net;

namespace Structure.DLL
{
    public class Gatewaye
    {
        SqlCommand cmd;
        internal int AddEditDepartment(Departments dep)
        {
            using(cmd=new SqlCommand("sp_addEdit_department",DBObject.Conn))
            {
                  DBObject.OpenConnetion();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@cateory_id",dep.Category_id);
            cmd.Parameters.AddWithValue("@category_name",dep.Category_name);
            int row = cmd.ExecuteNonQuery();
            return row;
            }
          
           
            
            
        }

        public static string SendSms(string phoneNo, string message)
        {
            
            string HtmlResult = "";
            try
            {
                string user=ConfigurationManager.AppSettings["apiuser"].ToString();
                string pass=ConfigurationManager.AppSettings["apipass"].ToString();
                string URI=ConfigurationManager.AppSettings["apiurl"].ToString();

                if(phoneNo.Substring(0,2) !="88")
                {
                    phoneNo = "88" + phoneNo;
                }
                string myParameters = "user=" + user + "&pass=" + pass + "&SMSText=" + message + "&GSM=" + phoneNo;

                using (WebClient wc = new WebClient())
                {
                    wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                    HtmlResult = wc.UploadString(URI, myParameters);
                }

            }
            catch (Exception ex)
            {
                
                throw ex;
            }
            return HtmlResult;
        }



        internal DataTable GetEmployeReport(string department, string employee_id)
        {
            using (cmd = new SqlCommand("rpt_sp_employeeinfo", DBObject.Conn))
            {
                try
                {
  
                    DBObject.OpenConnetion();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 10000;
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    cmd.Parameters.AddWithValue("@department", department);
                    cmd.Parameters.AddWithValue("@employee_id" , employee_id);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    return dt;
                }
                finally
                {

                    DBObject.CloseConnection();
                }
               

            }
        }
    }
}