﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Structure.Models
{
    public class Employee
    {
        public string Emp_id { get; set; }
        public string Emp_name { get; set; }
        public string Department { get; set; }
        public DateTime Join_date { get; set; }
    }
}