﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="employeeInfo.aspx.cs" Inherits="Structure.UI.employeeInfo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
    <form class="form-horizontal" id="form1" runat="server">
        <div class="container">
            <br />
            <br />
            <h3>Employee Information</h3> <asp:Label ID="lbl" runat="server"></asp:Label>
            <hr />
            <div class="form-group">
                <label class="control-label col-sm-2" for="email">Email:</label>
                <div class="col-sm-10">
                    <asp:TextBox ID="txtepmid" runat="server" class="form-control" placeholder="Employee Id"></asp:TextBox>

                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="pwd">Employee Name:</label>
                <div class="col-sm-10">
                     <asp:TextBox ID="txtempName" runat="server" class="form-control" placeholder="Employee Name"></asp:TextBox>
                </div>
            </div>
              <div class="form-group">
                <label class="control-label col-sm-2" for="pwd">Department:</label>
                <div class="col-sm-10">
                    <asp:DropDownList ID="dd" runat="server" CssClass="form-control"> </asp:DropDownList>
                </div>
            </div>
              <div class="form-group">
                <label class="control-label col-sm-2" for="pwd">Join Date:</label>
                <div class="col-sm-10">
                     <asp:TextBox ID="txtjoinDate" runat="server" class="form-control" TextMode="Date"></asp:TextBox>
                </div>
            </div>
               <div class="form-group">
                <label class="control-label col-sm-2" for="pwd">Join Date:</label>
                <div class="col-sm-10">
                    <asp:FileUpload ID="FileUpload1" runat="server" />
                </div>
            </div>
                <div class="form-group">
                <label class="control-label col-sm-2" for="pwd"></label>
                <div class="col-sm-10">
                    
                </div>
                    <asp:Image ID="Image1" runat="server" Height="190px" ImageUrl="~/images/noimage.jpg" Style="margin: 10px; border-radius: 5px; border: 1px solid #9BD8B8;" Width="160px" />
            </div>

     <%--       <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox">
                            Remember me</label>
                    </div>
                </div>
            </div>--%>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <asp:Button ID="tbnSave" runat="server" CssClass="btn btn-success btn-block" Text="SUBMIT" OnClick="tbnSave_Click" />
                </div>
            </div>

            <div class="row">
                <div class="col-sm-offset-2 col-sm-10">
                    <asp:GridView ID="gvEmployee" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                         PageSize="3" OnRowCommand="gvEmployee_RowCommand" OnPageIndexChanging="gvEmployee_PageIndexChanging">
                        <Columns>
                         
                            <asp:BoundField DataField="emp_id" HeaderText="Employee Id">
                                <ItemStyle HorizontalAlign="Center"/>
                                </asp:BoundField>
                             <asp:BoundField DataField="emp_name" HeaderText="Employee Id">
                                <ItemStyle HorizontalAlign="Center"/>
                                </asp:BoundField>
                            <asp:TemplateField HeaderText="Photo">
                                <ItemTemplate>
<%--                                    <img  src='<%#"data:Image/jpg;base64,"+ Convert.ToBase64String((byte[])Eval("photo")) %>' height="10" width="10"/>--%>
                                    <asp:Image ID="Image2" runat="server" ImageUrl='<%#"data:Image/jpg;base64,"+ Convert.ToBase64String((byte[])Eval("photo")) %>' Height="30" Width="40"/>
                                </ItemTemplate>
                            </asp:TemplateField>
                       <%--   <asp:ImageField DataImageUrlField="photo" HeaderText="Image"></asp:ImageField> --%> 
                               <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>
                                   <asp:LinkButton runat="server" ID="btnEdit" Text="Edit" CommandName="_edit" CommandArgument='<%#Bind("emp_id") %>' ></asp:LinkButton>
                                    <asp:LinkButton ID="btnDelete" runat="server" Text="Delete" CommandName="_delete" CommandArgument='<%#Bind("emp_id") %>'></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>

                </div>
               

            </div>
             <br />
                <br />

        </div>

    </form>
</body>
</html>
