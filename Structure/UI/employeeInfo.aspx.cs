﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using DAL.Gateway;
using System.IO;
namespace Structure.UI
{
    public partial class employeeInfo : System.Web.UI.Page
    {
        Transaction objTrans;
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                LoadDepartment();
                LoadDataGrid();
            }
        }

        private void LoadDataGrid()
        {
            objTrans = new Transaction();
           DataTable dt=objTrans.FetchData("Select *from employee_info");
            gvEmployee.DataSource=dt;
            gvEmployee.DataBind();
       
        }

      

        private void LoadDepartment()
        {
            objTrans = new Transaction();
            DataTable dt = objTrans.FetchData("Select *from department");
            dd.DataSource = dt;
            dd.DataValueField = "category_id";
            dd.DataTextField = "category_name";
            dd.DataBind();
            dd.Items.Insert(0,new ListItem("--Select Department--","0"));
            
        }

        protected void tbnSave_Click(object sender, EventArgs e)
        {
            if (txtepmid.Text != "" && FileUpload1.PostedFile.FileName != "")
            {
                //using (Transaction objTrans = new Transaction())
                //{
                //    int result = objTrans.ExecuteStoreProcedureCommadn("sp_addEdit_employee '" + txtempName.Text + "'", DBObject.Conn);
                //}
                int len = FileUpload1.PostedFile.ContentLength;
                byte[] photo = new byte[len];
                FileUpload1.PostedFile.InputStream.Read(photo, 0, len);



                using (SqlCommand cmd = new SqlCommand("sp_addEdit_employee", DBObject.Conn))
                {
                    try
                    {
                        DBObject.OpenConnetion();
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandTimeout = 10000;
                        cmd.Parameters.AddWithValue("@emp_id", txtepmid.Text);
                        cmd.Parameters.AddWithValue("@emp_name", txtempName.Text);
                        cmd.Parameters.AddWithValue("@department", dd.SelectedValue);
                        cmd.Parameters.AddWithValue("@join_date", txtjoinDate.Text);//Convert.ToDateTime(txtjoinDate.Text)
                        cmd.Parameters.AddWithValue("@photo",photo);
                      
                        cmd.ExecuteNonQuery();
                        tbnSave.Text = "UpDate";
                        lbl.Text = "Saved";
                        LoadDataGrid();
                    }
                    catch (Exception ex)
                    {

                        throw ex;
                    }
                    finally
                    {
                        DBObject.CloseConnection();
                    }

                }
            }
           
        }

        protected void gvEmployee_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "_edit")
            {
                fetch(e.CommandArgument.ToString());
            }
            if (e.CommandName == "_delete")
            {
                delete(e.CommandArgument.ToString());
            }

        }

        private void delete(string empid)
        {
            objTrans = new Transaction();
            int result = objTrans.Delete("Delete from employee_info where emp_id='" + empid + "'");
                if(result>0)
                {
                    lbl.Text="Deleted Successfully";
                    LoadDataGrid();
                }
        }

        private void fetch(string empId)
        {
            objTrans = new Transaction();
            DataTable dt = objTrans.FetchData("Select *from employee_info where emp_id='"+empId+"'");
            if(dt.Rows.Count>0)
            {
                txtepmid.Text=dt.Rows[0]["emp_id"].ToString();
                txtempName.Text = dt.Rows[0]["emp_name"].ToString();
                tbnSave.Text = "Update";
            }
        }

        protected void gvEmployee_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvEmployee.PageIndex = e.NewPageIndex;
            LoadDataGrid();

        }
    }
}