﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="sms.aspx.cs" Inherits="Structure.UI.sms" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>SMS</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
    <form id="form1" class="form-horizontal" runat="server">
        <div class="container">
            <br />
            <h3>Send Message </h3><asp:Label ID="lbl" runat="server"></asp:Label>
            <hr />
            <div class="form-group">
                <label class="control-label col-sm-2">Phone No:</label>
                <div class="col-sm-10">
                    <asp:TextBox ID="txtPhoneNo" runat="server" placeholder="Text Phone No" CssClass="form-control"></asp:TextBox>
                </div>

            </div>
            <div class="form-group">
                <label class="control-label col-sm-2"></label>
                <div class="col-sm-10">
                    <asp:TextBox ID="txtMessage" runat="server" CssClass="form-control" placeholder="Text Message"></asp:TextBox>
                </div>

            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <asp:Button ID="btnsend" runat="server" CssClass="btn btn-success btn-block" Text="SUBMIT" OnClick="btnsend_Click"/>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
