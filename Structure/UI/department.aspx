﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="department.aspx.cs" Inherits="Structure.UI.department" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
    <form class="form-horizontal" id="form1" runat="server">
        <div class="container">
            <br />
            <br />
            <h3>Employee Information</h3> <asp:Label ID="lbl" runat="server"></asp:Label>
            <hr />
            <div class="form-group">
                <label class="control-label col-sm-2" for="email">Category Id:</label>
                <div class="col-sm-10">
                    <asp:TextBox ID="txtid" runat="server" class="form-control" placeholder="Employee Id"></asp:TextBox>

                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="pwd">Category Name:</label>
                <div class="col-sm-10">
                     <asp:TextBox ID="txtCategoryname" runat="server" class="form-control" placeholder="Category Name"></asp:TextBox>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <asp:Button ID="tbnSave" runat="server" CssClass="btn btn-success btn-block" Text="SUBMIT" OnClick="tbnSave_Click" />
                </div>
            </div>
        </div>

    </form>
</body>
</html>